<?php

use Silex\Application;
use Silex\Provider\FormServiceProvider;

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// register global error and exception handlers
ErrorHandler::register();
ExceptionHandler::register();

// register service providers
$app->register(new Silex\Provider\DoctrineServiceProvider());
//$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app->register(new Silex\Provider\RoutingServiceProvider());

// register twig view providers
$app->register(new Silex\Provider\TwigServiceProvider(), array(
		'twig.path' => __DIR__ .'/views',
	));
$app->register(new Silex\Provider\AssetServiceProvider(), array(
		'assets.version'        => 'v1',
		'assets.version_format' => '%s?version=%s',
		'assets.named_packages' => array(
			'css'                  => array('version'                  => '1.0',
				'base_path'           => $app['ROOT'].'/css/'),
			'images'               => array('base_path'               => $app['ROOT'].'/img/'),
			'fonts'                => array('base_path'                => $app['ROOT'].'/fonts/'),
			'js'                   => array('base_path'                   => $app['ROOT'].'/js/')

		),
	));
$app->register(new FormServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
		'translator.messages' => array(),
	));
$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\MonologServiceProvider(), array(
		'monolog.logfile' => $app['monolog.logfile'],
		'monolog.name'    => 'Compta',
		'monolog.level'   => $app['monolog.level'],
	));
if ($app['monolog.level'] == 'debug') {
	$logger = new Doctrine\DBAL\Logging\DebugStack();
	$app['db.config']->setSQLLogger($logger);
	$app->error(

function (\Exception $e, $code) use ($app, $logger) {
			if ($e instanceof PDOException and count($logger->queries)) {
				// We want to log the query as an ERROR for PDO exceptions!
				$query = array_pop($logger->queries);
				$app['monolog']->err($query['sql'], array(
						'params' => $query['params'],
						'types'  => $query['types'],
					));
			}
		});
	$app->after(function (Request $request, Response $response) use ($app, $logger) {
			// Log all queries as DEBUG.
			foreach ($logger->queries as $query) {
				$app['monolog']->debug($query['sql'], array(
						'params' => $query['params'],
						'types'  => $query['types'],
					));
			}
		});
}

//register services
$app['dao.group']   = new Compta\DAO\GroupDAO($app['db']);
$app['dao.user']    = new Compta\DAO\UserDAO($app['db']);
$app['dao.depense'] = new Compta\DAO\DepenseDAO($app['db']);
$app['dao.user']->setDepenseDAO($app['dao.depense']);

// register JSON data decoder for JSON requests
$app->before(function (Request $request, Application $app) {
		if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
			$date = date('[Y-m-d H:i:s] ', time());
			$prefix = 'Compta.JSON.in: ';
			$json = array_reverse(explode("\n", $request->getContent()))[0];
			$file = fopen($app['monolog.logfile'], 'a');
			fwrite($file, $date.$prefix.$json."\n");
			fclose($file);
			$data = json_decode($request->getContent(), true);
			$request->request->replace(is_array($data)?$data:array());
		}
	});

?>
