<?php

/********************************************* Documentation********************************************/

$app->get(
	'/doc',

	function () {
		$response = new Symfony\Component\HttpFoundation\Response();
		$response->headers->set('Location', 'docs/index.html');
		return $response;
	}
);

/********************************************* REST API ***********************************************/

$app->get(
	'/api/groups',
	'Compta\Controller\APIControllerRead::getGroups'
);

$app->get(
	'/api/users',
	'Compta\Controller\APIControllerRead::getUsersAll'
);

$app->get(
	'/api/depenses',
	'Compta\Controller\APIControllerRead::getDepensesAll'
);

$app->get(
	'/api/group/{group_id}/users',
	'Compta\Controller\APIControllerRead::getUsers'
);

$app->get(
	'/api/group/{group_id}/depenses',
	'Compta\Controller\APIControllerRead::getDepenses'
);

$app->get(
	'/api/logout',
	'Compta\Controller\APIControllerAdmin::logout'
);

$app->post(
	'/api/login',
	'Compta\Controller\APIControllerAdmin::login'
);

$app->post(
	'/admin/group',
	'Compta\Controller\APIControllerCreate::addGroup'
);

$app->post(
	'/admin/user',
	'Compta\Controller\APIControllerCreate::addUser'
);

$app->post(
	'/admin/depense',
	'Compta\Controller\APIControllerCreate::addDepense'
);

$app->delete(
	'/admin/group/{id}',
	'Compta\Controller\APIControllerDelete::deleteGroup'
);

$app->delete(
	'/admin/user/{id}',
	'Compta\Controller\APIControllerDelete::deleteUser'
);

$app->delete(
	'/admin/depense/{id}',
	'Compta\Controller\APIControllerDelete::deleteDepense'
);

/********************************************* GUI ***********************************************/

/*********** HOME PAGE **************/
$app->get('/', 'Compta\Controller\GUIController::getHomePage')->bind('home');
$app->get('/home', 'Compta\Controller\GUIController::getHomePage');

/*********** CONNECTION **************/
$app->post(
	'/connect',
	'Compta\Controller\GUIControllerLogging::login'
)->bind('login');
$app->get(
	'/connect',
	'Compta\Controller\GUIControllerLogging::login'
);

$app->get('/disconnect',
	'Compta\Controller\GUIControllerLogging::logout'

)->bind('logout');

/*********** STATS **************/
$app->get('/statistics',
	'Compta\Controller\GUIController::getStatisticsPage'

)->bind('statistiques');

$app->get('/stats',
	'Compta\Controller\GUIController::getParitarianStatisticsPage'

)->bind('paritarian');

/*********** DEBTS **************/
$app->get('/depenses',
	'Compta\Controller\GUIController::getDepensesPage'

)->bind('depenses');
$app->post('/depenses',
	'Compta\Controller\GUIController::getDepensesPage'

);

/*********** DEBTS **************/
$app->get('/depense/{id}',
	'Compta\Controller\GUIController::editDepense'

)->bind('editDepense');
$app->post('/depense/{id}',
	'Compta\Controller\GUIController::editDepense'

);
$app->get('/depense/delete/{id}',
	'Compta\Controller\GUIController::deleteDepense'

)->bind('deleteDepense');
$app->post('/depense/delete/{id}',
	'Compta\Controller\GUIController::deleteDepense'

);

/********** USERS ***************/
$app->get('/users',
	'Compta\Controller\GUIController::getUsers'

)->bind('users');

$app->get('/user/{id}',
	'Compta\Controller\GUIController::editUser'

)->bind('editUser');
$app->post('/user/{id}',
	'Compta\Controller\GUIController::editUser'

);
$app->get('/user/delete/{id}',
	'Compta\Controller\GUIController::deleteUser'

)->bind('deleteUser');
$app->post('/user/delete/{id}',
	'Compta\Controller\GUIController::deleteUser'

);
?>
