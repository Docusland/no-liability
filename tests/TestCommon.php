<?php

namespace Compta\Tests;
include "testData.php";

trait TestCommon {

	public function createApplication() {
		global $TESTS;
		$app         = new \Silex\Application();
		$app['ROOT'] = $TESTS["root_url"];
		require __DIR__ .'/../app/config/dev.php';
		require __DIR__ .'/../app/app.php';
		require __DIR__ .'/../app/routes.php';
		$app['debug'] = true;
		unset($app['exception_handler']);
		return $app;
	}

}

?>
