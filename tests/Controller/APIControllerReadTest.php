<?php

use Silex\WebTestCase;

class APIControllerReadTest extends WebTestCase {

	use Compta\Tests\TestCommon;

	public function testGetGroups() {
		$client  = $this->createClient();
		$crawler = $client->request('GET', '/api/groups');
		$this->assertTrue($client->getResponse()->isOk());
	}

	public function testGetUsers() {
		global $TESTS;
		$client  = $this->createClient();
		$crawler = $client->request('GET', '/api/group/'.$TESTS['group_id'].'/users');
		$this->assertTrue($client->getResponse()->isOk());
	}

	public function testGetUsersAll() {
		global $TESTS;
		$client  = $this->createClient();
		$crawler = $client->request('GET', '/api/users');
		$this->assertTrue($client->getResponse()->isOk());
	}

	public function testGetDepenses() {
		global $TESTS;
		$client  = $this->createClient();
		$crawler = $client->request('GET', '/api/group/'.$TESTS['group_id'].'/depenses');
		$this->assertTrue($client->getResponse()->isOk());
	}

	public function testGetDepensesAll() {
		global $TESTS;
		$client  = $this->createClient();
		$crawler = $client->request('GET', '/api/depenses');
		$this->assertTrue($client->getResponse()->isOk());
	}

}

?>
