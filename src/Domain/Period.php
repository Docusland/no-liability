<?php

namespace Compta\Domain;

class Period {

	/** @var int    The id of the current period. */
	private $id;
	/** @var int The user involved for the given period. */
	private $user_id;
	/** @var Date   The day of arrival of the current period. */
	private $startDate;
	/** @var Date   The day of depart of the current period. */
	private $endDate;

	public function __toString() {
		return "Period :{id:".$this->user_id.", name:".$this->startDate.", name:".$this->endDate."}";
	}
	/**
	 * Returns the id of the current period.
	 *
	 * @return int The id of the current period.
	 */
	public function getId() {return $this->id;}

	/**
	 * Returns the id of the user.
	 *
	 * @return string The id of the user.
	 */
	public function getUserId() {return $this->user_id;}


	/**
	 * Returns the start date of the current period has initiated.
	 *
	 * @return date The start date of the period.
	 */
	public function getStartDate() {return $this->startDate;}

	/**
	 * Returns the end date of the current period has initiated.
	 *
	 * @return date The end date of the period.
	 */
	public function getEndDate() {return $this->endDate;}

  /**
	 * Returns the difference in days between the end date and the start date.
	 *
	 * @return date The difference in days.
	 */
	public function getDureeSejour() {
		$begin    = date_create_from_format("Ymd", $this->startDate);
		$end      = date_create_from_format("Ymd", $this->endDate);
		$datediff = $end->diff($begin)->format("%a");

		return $datediff;
	}
	/**
	 * Sets the id of the current period.
	 *
	 * @param int|string The new id of the period, should be numeric and
	 *                   greater than 0.
	 *
	 * @return self|null The current period if the param is valid, null otherwise.
	 */
	public function setId($id) {
		$id = (int) $id;
		if ($id <= 0) {return NULL;
		}

		$this->id = $id;
		return $this;
	}

	/**
	 * Sets the id of the user.
	 *
   * @param int|string The id of the user, should be numeric and
	 *                   greater than 0.
	 *
	 * @return self|null The current period if the param is valid, null otherwise.
	 */
   public function setUserId($id) {
 		$id = (int) $id;
 		// if ($id <= 0) {
    //   throw new Exception("Creating a period with no related user! $id")
    //   return NULL;
 		// }

 		$this->id = $id;
 		return $this;
 	}


	/**
	 * Sets the start date of the current period has initiated.
	 *
	 * @param date The start date of the period in format YYYYMMDD.
	 */
	public function setStartDate($date) {
		$this->startDate = $date;
	}

	/**
	 * Sets the end date of the current period has initiated.
	 *
	 *  @param date The end date of the period in format YYYYMMDD.
	 */
	public function setEndDate($date) {
		$this->endDate = $date;
	}

}

?>
