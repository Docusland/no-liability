<?php
	
	namespace Compta\Domain;
	
	class Group {
		
		/** @var int    The id of the current object. */
		private $id;
		/** @var string The name of the current object. */
		private $name;
		
		/**
		 * Returns the id of the current object.
		 *
		 * @return int The id of the current object.
		 */
		public function getId() { return $this->id; }
		/**
		 * Returns the name of the current object.
		 *
		 * @return string The name of the current object.
		 */
		public function getName() { return $this->name; }
		
		/**
		 * Sets the id of the current object.
		 *
		 * @param int|string The new id of the current object, should be numeric and
		 *                   greater than 0.
		 *
		 * @return self|null The current object if the param is valid, null
		 *                   otherwise.
		 */
		public function setId($id) {
			$id = (int) $id;
			if ($id <= 0) return NULL;
			$this->id = $id;
			return $this;
		}
		
		/**
		 * Sets the name of the current object.
		 *
		 * @param string The new name of the current object, should be no longer
		 *               than 255 characters and should not be empty.
		 *
		 * @return self|null The current object if the param is valid, null
		 *                   otherwise.
		 */
		public function setName($name) {
			$name = (string) $name;
			$length = strlen($name);
			if ($length = 0 || $length > 255) return NULL;
			$this->name = $name;
			return $this;
		}
		
	}
	
?>
