<?php

namespace Compta\Domain;

class Depense {

	/** @var int    The id of the current depense. */
	private $id;
	/** @var int    The amount of the current depense. */
	private $montant;
	/** @var int    The date of the current depense in timestamp format. */
	private $date;
	/** @var string The name of the current depense. */
	private $name;
	/** @var int    The id of the group for the current depense. */
	private $group_id;
	/** @var int    The id of the user who initiated the current depense. */
	private $user_id;
	/** @var int[]  The ids of the users who tok part in the current depense. */
	private $users;
	/** @var bool  No meat is in the current depense. default: false*/
	private $isVegetarian;
	/** @var int    The number of days to spread the current depense. */
	private $nbNights;

	public function __toString() {
		return "Depense{id:".$this->getId().", name:'".$this->getName().", montant:".$this->getMontant()."}";
	}
	/**
	 * Returns the id of the current depense.
	 *
	 * @return int The id of the current depense.
	 */
	public function getId() {return $this->id;}

	/**
	 * Returns the amount of the current depense.
	 *
	 * @return int The amount of the current depense.
	 */
	public function getMontant() {return (float) $this->montant;}

	/**
	 * Returns the date of the current depense.
	 *
	 * @return int The date of the current depense in timestamp format.
	 */
	public function getDate() {return $this->date;}

	public function getDateFrenchFormat() {

		return \Compta\Tools\Utilities::toFrenchFormat($this->date);
	}
	/**
	 * Returns the name of the current depense.
	 *
	 * @return string The name of the current depense.
	 */
	public function getName() {return $this->name;}

	/**
	 * Returns the id of the group for the current depense.
	 *
	 * @return int The id of the group for the current depense.
	 */
	public function getGroupId() {return $this->group_id;}

	/**
	 * Returns the id of the user who initiated the current depense.
	 *
	 * @return int The id of the user who initiated the current depense.
	 */
	public function getUserId() {return $this->user_id;}

	/**
	 * Returns the number of nights to spread the current depense.
	 *
	 * @return int The number of nights to spread the current depense.
	 */
	public function getNbNights() {return $this->nbNights;}
	/**
	 * Returns the ids of the users who took part in the current depense.
	 *
	 * @return int[] The list of ids of the users who took part the current
	 *               depense.
	 */
	public function getUsers() {return $this->users;}

	/**
	 * Sets the id of the current depense.
	 *
	 * @param int|string The new id of the current depense, should be numeric
	 *                   and greater than 0.
	 *
	 * @return self|null The current depense if the param is valid, null
	 *                   otherwise.
	 */
	public function setId($id) {
		$id = (int) $id;
		if ($id <= 0) {return NULL;
		}

		$this->id = $id;
		return $this;
	}

	/**
	 * Sets the amount of the current depense.
	 *
	 * @param float|int|string The new amount of the current depense, should be
	 *                         numeric and greater than 0.
	 *
	 * @return self|null The current depense if the param is valid, null
	 *                   otherwise.
	 */
	public function setMontant($montant) {
		$montant = (float) $montant;
		if ($montant <= 0) {return NULL;
		}

		$this->montant = $montant;
		return $this;
	}

	/**
	 * Sets the date of the current depense.
	 *
	 * @param int|string The new date of the current depense, should be numeric
	 *                   and greater than 0.
	 *
	 * @return self|null The current depense if the param is valid, null
	 *                   otherwise.
	 */
	public function setDate($date) {
		$date = (int) $date;
		if ($date < 0) {return NULL;
		}

		$this->date = $date;
		return $this;
	}

	/**
	 * Sets the name of the current depense.
	 *
	 * @param string The new name of the current depense, should be no longer
	 *               than 255 characters and should not be empty.
	 *
	 * @return self|null The current depense if the param is valid, null
	 *                   otherwise.
	 */
	public function setName($name) {
		$name   = (string) $name;
		$length = strlen($name);
		if ($length = 0 || $length > 255) {return NULL;
		}

		$this->name = $name;
		return $this;
	}

	/**
	 * Sets the id of the group for the current depense.
	 *
	 * @param int|string The new id of the group for the current depense, should
	 *                   be numeric and greater than 0.
	 *
	 * @return self|null The current depense if the param is valid, null
	 *                   otherwise.
	 */
	public function setGroupId($group_id) {
		$group_id = (int) $group_id;
		if ($group_id <= 0) {return NULL;
		}

		$this->group_id = $group_id;
		return $this;
	}

	/**
	 * Sets the id of the user who initiated the current depense.
	 *
	 * @param int|string The new id of the user who initiated the current
	 *                   depense, should be numeric and greater than 0.
	 *
	 * @return self|null The current depense if the param is valid, null
	 *                   otherwise.
	 */
	public function setUserId($user_id) {
		if ($user_id instanceof User) {
			$this->user_id = $user_id->getId();
		} else {
			$user_id = (int) $user_id;
			if ($user_id <= 0) {
				return NULL;
			}

			$this->user_id = $user_id;
		}
		return $this;
	}

	/**
	 * Returns the number of nights to spread the current depense.
	 *
	 * @return int The number of nights to spread the current depense.
	 */

	 /**
		* Sets the the number of nights to spread the current depense.
		*
		* @param int|string The the number of nights to spread the current
		*                   depense, should be numeric and greater than 0 or ?.
		*
		* @return self|null The current depense if the param is valid, null
		*                   otherwise.
		*/
	 public function setNbNights($nbNights) {
			$this->nbNights = $nbNights;

		 return $this;
	 }


	/**
	 * Sets the ids of the users who took part in the current depense.
	 *
	 * @param mixed[] The new list of ids identifying the users who took part in
	 *                the current depense, all should be numeric and greater
	 *                than 0.
	 *
	 * @return self|null The current depense if the param is valid, null
	 *                   otherwise.
	 */
	public function setUsers(array $users) {
		if (count($users) == 0) {$this->users = NULL;
		} else {
			foreach ($users as $user) {
				$user = (int) $users;
				if ($user <= 0) {return NULL;
				}
			}

			$this->users = $users;
		}
		return $this;
	}

	/**
	 * Remove an user from the list of users who took part in the current
	 * depense.
	 *
	 * @param int The id of the user to remove from the list.
	 *
	 * @return self The current depense.
	 */
	public function removeUser($user_id) {
		$users     = $this->getUsers();
		$users_new = [];
		foreach ($users as $id)
		if ($id != $user_id) {$users_new[] = $id;
		}

		$this->setUsers($users_new);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIsVegetarian() {
		return $this->isVegetarian;
	}

	/**
	 * @param mixed $isVegetarian
	 *
	 * @return self
	 */
	public function setIsVegetarian($isVegetarian) {
		$this->isVegetarian = $isVegetarian;

		return $this;
	}
}

?>
