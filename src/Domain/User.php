<?php

namespace Compta\Domain;

class User {

	/** @var int    The id of the current user. */
	private $id;
	/** @var string The name of the current user. */
	private $name;
	/** @var string The color of the current user. */
	private $color;
	/** @var int[]  The list of groups for the current user. */
	private $groups;
	/** @var int[]  The list of depenses the current user has initiated. */
	private $depenses;
	/** @var Date   The day of arrival of the current user. */
	// private $startDate;
	/** @var Date   The day of depart of the current user. */
	// private $endDate;
	private $isVegetarian;
	private $periods;

	public function __toString() {
		$sPeriods =join(",",$this->periods);


		return "User :{".$this->id." :".$this->name."}"; // periods: $sPeriods
	}
	/**
	 * Returns the id of the current user.
	 *
	 * @return int The id of the current user.
	 */
	public function getId() {return $this->id;}

	/**
	 * Returns the name of the current user.
	 *
	 * @return string The name of the current user.
	 */
	public function getName() {return $this->name;}

	/**
	 * Returns the color of the current user.
	 *
	 * @return string The color of the current user.
	 */
	public function getColor() {return $this->color;}

	/**
	 * Returns the list of groups for the current user.
	 *
	 * @return int[] The list of groups for the current user.
	 */
	public function getGroups() {return $this->groups;}

	/**
	 * Returns the list of depenses the current user has initiated.
	 *
	 * @return int[] The list of depenses the current user has initiated.
	 */
	public function getDepenses() {return $this->depenses;}

	/**
	 * Returns the smallest start date stored in the periods of the current user.
	 *
	 * @return date The start date of the user.
	 */
	 public function getStartDate() {
		 $startDate = date('Ymd');
		 foreach ($this->periods as $key => $period) {
		 	if( $period->getStartDate() < $startDate) $startDate = $period->getStartDate() ;
		 }
		 return $startDate;
	 }

	/**
	 * Returns the end date of the current user has initiated.
	 *
	 * @return date The end date of the user.
	 */
	public function getEndDate() {
		$endDate = 0;
		foreach ($this->periods as $key => $period) {
		 if( $period->getEndDate() > $endDate) $endDate = $period->getEndDate() ;
		}
		return $endDate;
	}

	public function isPresent($day){
			$isPresent = false;
		foreach ($this->periods as $key => $period) {
		 if( $period->getEndDate() > $day && $period->getStartDate() <= $day)
		 	$isPresent = true;
		}
		return $isPresent;
	}

	public function getIsVegetarian() {return $this->isVegetarian;}

	public function getDureeSejour() {
		$durees=[];
		foreach ($this->getPeriods() as $key => $period) {
			$begin    = date_create_from_format("Ymd", $period->getStartDate());
			$end      = date_create_from_format("Ymd", $period->getEndDate());
			$datediff = $end->diff($begin)->format("%a");
			$durees[]=$datediff;
			// error_log($datediff);
		}


		return $durees;
	}

	public function getPeriods(){
		// if(count($this->periods)>0 ) return $this->periods[0];
		return $this->periods;
	}
	public function setPeriods($periods){
		$this->periods = $periods;
		return $this;
	}
	/**
	 * Sets the id of the current user.
	 *
	 * @param int|string The new id of the current user, should be numeric and
	 *                   greater than 0.
	 *
	 * @return self|null The current user if the param is valid, null otherwise.
	 */
	public function setId($id) {
		$id = (int) $id;
		if ($id <= 0) {return NULL;
		}

		$this->id = $id;
		return $this;
	}

	/**
	 * Sets the name of the current user.
	 *
	 * @param string The new name of the current user, should be no longer than
	 *               255 characters and should not be empty.
	 *
	 * @return self|null The current user if the param is valid, null otherwise.
	 */
	public function setName($name) {
		$name   = (string) $name;
		$length = strlen($name);
		if ($length = 0 || $length > 255) {return NULL;
		}

		$this->name = $name;
		return $this;
	}

	/**
	 * Sets the color of the current user.
	 *
	 * @param string The new color of the current user, should be no longer than
	 *               255 characters and should not be empty.
	 *
	 * @return self|null The current user if the param is valid, null otherwise.
	 */
	public function setColor($color) {
		$color  = (string) $color;
		$length = strlen($color);
		if ($length = 0 || $length > 255) {return NULL;
		}

		$this->color = $color;
		return $this;
	}

	/**
	 * Sets the list of groups for the current user.
	 *
	 * @param int[] The new list of groups for the current user, as group ids.
	 *
	 * @return self|null The current user if the param is valid, null otherwise.
	 */
	public function setGroups(array $groups) {
		if (count($groups) == 0) {$this->groups = NULL;
		} else {
			foreach ($groups as $group) {
				$group = (int) $groups;
				if ($group <= 0) {return NULL;
				}
			}

			$this->groups = $groups;
		}
		return $this;
	}

	/**
	 * Sets the list of depenses initiated by the current user.
	 *
	 * @param int[] The new list of depenses for the current user, as depense
	 *              ids.
	 *
	 * @return self|null The current user if the param is valid, null otherwise.
	 */
	public function setDepenses(array $depenses) {
		foreach ($depenses as $depense) {
			$depense = (int) $depense;
			if ($depense <= 0) {return NULL;
			}
		}

		$this->depenses = $depenses;
		return $this;
	}

	/**
	 * Remove a group from the list of groups for the current user.
	 *
	 * @param int The id of the group to remove from the list.
	 *
	 * @return self The current user.
	 */
	public function removeGroup($group_id) {
		$groups     = $this->getGroups();
		$groups_new = [];
		foreach ($groups as $id)
		if ($id != $group_id) {$groups_new[] = $id;
		}

		$this->setGroups($groups_new);
		return $this;
	}

	/**
	 * Sets the start date of the current user has initiated.
	 *
	 * @param date The start date of the user in format YYYYMMDD.
	 */
	// public function setStartDate($date) {
	// 	$this->startDate = $date;
	// }

	/**
	 * Sets the end date of the current user has initiated.
	 *
	 *  @param date The end date of the user in format YYYYMMDD.
	 */
	// public function setEndDate($date) {
	// 	$this->endDate = $date;
	// }

	public function setIsVegetarian($vegetarian) {
		$this->isVegetarian = $vegetarian;
	}

}

?>
