<?php
namespace Compta\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Compta\Form\PeriodType;
use Compta\Domain\Period;
class UserType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$periods = $options['periods'];
		$builder->add('id', HiddenType::class );
		$builder->add('name');

		// $builder->add('startdate', NumberType::class , array(
		// 		'required'    => true,
		// 		'constraints' => array(
		// 			new Assert\NotBlank(),
		// 			new Assert\Length(array(
		// 					'min' => 8, 'max' => 8,
		// 				))),
		// 	));
		// 	$builder->add('enddate', NumberType::class , array(
		// 			'required'    => true,
		// 			'constraints' => array(
		// 				new Assert\NotBlank(),
		// 				new Assert\Length(array(
		// 						'min' => 8, 'max' => 8,
		// 					))),
		// 		));

				$default_period = new Period();

				$default_period->setStartDate(date('Ymd'));
				$default_period->setEndDate(date('Ymd'));

				$builder->add('periods', CollectionType::class, array(
				    'entry_type' => PeriodType::class,
				    'allow_add' => true,
				    // 'prototype' => true,
				    // 'prototype_data' =>$default_period ,
				));
				$builder->add('color');
				$builder->add('isVegetarian', NumberType::class, array(
				    'label'    => 'Is Vegetarian?',
						'required'    => true
				));
		// $builder->add('periods', ChoiceType::class , [
		// 		'choices'      => $periods,
		// 		'choice_label' => function ($payeur, $key, $index) {
		// 			/** @var Category $category */
		// 			return strtoupper($payeur->getName());
		// 		},
		// 		'choice_value'   => 'id',
		// 		'choice_attr'    => function ($payeur, $key, $index) {
		// 			return ['class' => 'category_'.strtolower($payeur->getName())];
		// 		},
		// 		'multiple' => true,
		// 		'expanded' => true,
		// 		'data'     => $periods,
		// 	]);
		$builder->add('submit', SubmitType::class , [
				'label' => 'Valider',
			]);

	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
				'data_class' => 'Compta\Domain\User',
				'periods'      => [],
			));
	}

	public function getName() {

		return 'compta_domain_user';

	}
	public function getExtendedType() {
		return 'form';
	}

}
