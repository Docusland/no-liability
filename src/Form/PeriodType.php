<?php
namespace Compta\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PeriodType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('id', HiddenType::class );
    $builder->add('user_id', HiddenType::class );
		$builder->add('startDate', NumberType::class , array(
				'required'    => true,
				'constraints' => array(
					new Assert\NotBlank(),
					new Assert\Length(array(
							'min' => 8, 'max' => 8,
						))),
			));
			$builder->add('endDate', NumberType::class , array(
					'required'    => true,
					'constraints' => array(
						new Assert\NotBlank(),
						new Assert\Length(array(
								'min' => 8, 'max' => 8,
							))),
				));


	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
				'data_class' => 'Compta\Domain\Period'
			));
	}

	public function getName() {

		return 'compta_domain_period';

	}
	public function getExtendedType() {
		return 'form';
	}

}
