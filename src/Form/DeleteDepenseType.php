<?php
namespace Compta\Form;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeleteDepenseType extends AbstractType {
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('id', HiddenType::class , array(
				"required" => true))
				->add('name', TextType::class,[
					'disabled' => true,
					'label' => ' ',
				])
				->add('montant', NumberType::class ,[
					'disabled' => true,
					'label' => ' ',
				])
			->add('submit', SubmitType::class , [
				'label' => 'Supprimer',
			]);
	}
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
				'data_class' => 'Compta\Domain\Depense',
				'users'      => [],
			));
	}

	public function getName() {

		return 'compta_domain_depense';

	}
	public function getExtendedType() {
		return 'form';
	}

}
?>
