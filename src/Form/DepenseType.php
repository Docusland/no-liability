<?php
namespace Compta\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class DepenseType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$this->users = $options['users'];
		$builder->add('id', HiddenType::class );
		$builder->add('user_id', ChoiceType::class , [

				'choices'      => $this->users,
				'choice_label' => function ($payeur, $key, $index) {
					return strtoupper($payeur->getName());
				},
				'label' => "Payeur",
				'choices_as_values' => true,
				'choice_value' =>  function($payeur){

						if(!is_null($payeur) && !is_string($payeur)){
						$object = $payeur->getId();
						return (string) $object;
					} else if(is_string($payeur)){
							return $this->users[$payeur]->getId();
						}
				},
				'constraints'  => array(
					new Assert\NotBlank(),
				),

			]);
		$builder->add('name',TextType::class, [
			'label'=>'Intitulé',
		]);

		$builder->add('date', NumberType::class , array(
				'required'    => true,
				'constraints' => array(
					new Assert\NotBlank(),
					new Assert\Length(array(
							'min' => 8, 'max' => 8,
						))),
			))
			->add('montant', NumberType::class , array(
				'scale'       => 2,
				'empty_data'  => 0,
				'required'    => true,
				'constraints' => array(
					new Assert\NotBlank(),
					new Assert\Length(array(
							'min' => 1, 'max' => 10,
						))),
			));
		$builder->add('isVegetarian', ChoiceType::class , array(
				'choices'    => array("Non"    => false, "Oui"    => true),
				'empty_data' => 0,
				'empty_data' => false,
				'label'=>'Végétarien ?',
			));
			$builder->add('nbNights', ChoiceType::class, array(
				'choices'     => array("Automatique" => "0","One Shot" => "1","2 jours" =>"2","3 jours" => "3","4 jours" => "4"),
				'label'=>'Durée',
			));
		$builder->add('users', ChoiceType::class , [
				'choices'      => $this->users,
				'choice_label' => function ($payeur, $key, $index) {
					/** @var Category $category */
					return strtoupper($payeur->getName());
				},
				'choice_value'   => 'id',
				'choice_attr'    => function ($payeur, $key, $index) {
					return ['class' => 'category_'.strtolower($payeur->getName())];
				},
				'multiple' => true,
				'expanded' => true,
				'label'=>'Concernés',
				'choices_as_values' => true,
				'choice_value' =>  function($payeur){

						if(!is_null($payeur) && !is_string($payeur)){
						$object = $payeur->getId();
						return (string) $object;
					} else if(is_string($payeur)){
							return $this->users[$payeur]->getId();
						}
				},

			]);
		$builder->add('submit', SubmitType::class , [
				'label' => 'Sauver',
			]);

	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
				'data_class' => 'Compta\Domain\Depense',
				'users'      => [],
			));
	}

	public function getName() {

		return 'compta_domain_depense';

	}
	public function getExtendedType() {
		return 'form';
	}

}
