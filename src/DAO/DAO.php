<?php
	
	namespace Compta\DAO;
	
	use Doctrine\DBAL\Connection;
	
	abstract class DAO 
	{
		
		/** @var Connection The current link to the database. */
		private $db;
		
		/**
		 * Sets the link to the database.
		 *
		 * @param Connection An open link to the database.
		 */
		public function __construct(Connection $db) { $this->db = $db; }
		
		/**
		 * Returns the link to the database.
		 *
		 * @return Connection The current link to the database.
		 */
		protected function getDb() { return $this->db; }
		
		/**
		 * Returns an instance of the given class.
		 *
		 * @param int|string An object id as an int, or an object name as a string.
		 * @param string     The name of the table where the class instances are
		 *                   stored.
		 * @param string     The class name, as a full path including the namespace.
		 *
		 * @return object An instance of the given class.
		 */
		protected function getObject($info, $table, $class) {
			$where = (is_numeric($info)) ? 'id = :info' : 'name = :info' ;
			$query = $this->getDb()->createQueryBuilder();
			$query->select('*')
			      ->from($table)
			      ->where($where)
			      ->setParameter(':info', $info);
			$statement = $query->execute();
			$statement->setFetchMode(\PDO::FETCH_CLASS, $class);
			return $statement->fetch();
		}
		
	}
	
?>
