<?php

namespace Compta\DAO;

use Compta\Domain\User;
use Compta\Domain\Period;

class UserDAO extends DAO {
	/** @var DepenseDAO The current instance of DepenseDAO. */
	private $depenseDAO;

	/**
	 * Returns the current instance of DepenseDAO.
	 *
	 * @return DepenseDAO The current instance of DepenseDAO.
	 */
	public function getDepenseDAO() {return $this->depenseDAO;}

	/**
	 * Sets the current instance of DepenseDAO.
	 *
	 * @param DepenseDAO An instance of DepenseDAO.
	 */
	public function setDepenseDAO(DepenseDAO $depenseDAO) {
		$this->depenseDAO = $depenseDAO;
	}

	/**
	 * Returns an instance of User.
	 *
	 * @param int|string An user id as an int, or an user name as a string.
	 *
	 * @return User An instance of User.
	 */
	public function get($info) {
		$user = $this->getObject($info, 'users', 'Compta\Domain\User');
		if (!$user) {

			return false;
		}

		//Manage related groups
		$query = $this->getDb()->createQueryBuilder();
		$query->select('*')
		      ->from('mapping_groups')
		      ->where('user_id = :user_id')
		      ->setParameter(':user_id', $user->getId());
		$answer = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		$groups = [];
		foreach ($answer as $row) {
			$groups[] = $row['group_id'];
		}
		$user->setGroups($groups);

		//Manage related periods
		$query = $this->getDb()->createQueryBuilder();
		$query->select('*')
		      ->from('user_has_periods')
		      ->where('user_id = :user_id')
					->orderBy('startDate','DESC')
		      ->setParameter(':user_id', $user->getId());
		$answer = $query->execute()->fetchAll(\PDO::FETCH_CLASS,'Compta\Domain\Period');
		$user->setPeriods($answer);
		return $user;
	}

	/**
	 * Returns the list of users in a given group.
	 *
	 * @param int $group_id The id of the group.
	 *
	 * @return User[] A list of User instances.
	 */
	public function findByGroup($group_id) {
		$query = $this->getDb()->createQueryBuilder();
		$query->select('*')
		      ->from('mapping_groups')
		      ->where('group_id = :group_id')
		      ->setParameter(':group_id', $group_id);
		$answer                           = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		$users                            = [];
		foreach ($answer as $row)$users[] = $this->get($row['user_id']);
		return $users;
	}

	/**
	 * Returns the list of all users.
	 *
	 * @return User[] A list of User instances.
	 */
	public function findAll() {
		$query = $this->getDb()->createQueryBuilder();
		$query->select('*')
		      ->from('users');
		$answer                           = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		$users                            = [];
		foreach ($answer as $row){
			$users[] = $this->get($row['id']);
		};
		return $users;
	}

	/**
	 * Create a new user in database, or update an existing one.
	 *
	 * $param User An instance of User.
	 */
	public function save(User $user) {
		// create/update entry in 'users' table
		$data = array(
			'name'       => $user->getName(),
			'color'      => $user->getColor(),
			// 'startDate'  => $user->getStartDate(),
			// 'endDate'    => $user->getEndDate(),
			'isVegetarian' => $user->getIsVegetarian()
		);
		$id = $user->getId();
		if ($id != NULL) {
			$this->getDb()->update('users', $data, array('id' => $id));
		} else {
			$this->getDb()->insert('users', $data);
			$id = $this->getDb()->lastInsertId();
			$user->setId($id);
		}
		// get entries from 'mapping_groups' table
		$query = $this->getDb()->createQueryBuilder();
		$query->select('*')
		      ->from('mapping_groups')
		      ->where('user_id = :user_id')
		      ->setParameter(':user_id', $user->getId());
		$answer = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		$groups = $user->getGroups();
		// create entries in 'mapping_groups' table
		foreach ($groups as $group) {
			$found = false;
			foreach ($answer as $row)
			if ($row['group_id'] == $group) {$found = true;
			}

			if (!$found) {
				$this->getDb()->insert('mapping_groups', array(
						'user_id'  => $id,
						'group_id' => $group,
					));
			}
		}

		// remove entries in 'mapping_groups' table
		foreach ($answer as $row) {
			$found = false;
			foreach ($groups as $group)
			if ($row['group_id'] == $group) {$found = true;
			}

			if (!$found) {
				$this->getDb()->delete('mapping_groups', array(
						'user_id'  => $id,
						'group_id' => $row['group_id'],
					));
			}
		}
	}

	/**
	 * Delete an user from database, and all subsequently unreferenced depenses.
	 *
	 * $param int The id of the user to delete.
	 */
	public function delete($id) {
		$this->getDb()->delete('users', array('id'               => $id));
		$this->getDb()->delete('mapping_groups', array('user_id' => $id));
		$this->getDb()->delete('user_has_periods', array('user_id' => $id));
		$this->getDepenseDAO()->deleteByUser($id);
	}

	/**
	 * Remove given group from all users groups list, and all subsequently
	 * unreferenced users.
	 *
	 * $param int The id of the group to remove.
	 */
	public function removeFromGroup($group_id) {
		// get entries from 'mapping_groups' table
		$query = $this->getDb()->createQueryBuilder();
		$query->select('*')
		      ->from('mapping_groups')
		      ->where('group_id = :group_id')
		      ->setParameter(':group_id', $group_id);
		$answer = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
		// remove unreferenced entries from 'users' table
		foreach ($answer as $row) {
			$id   = $row['user_id'];
			$user = $this->get($id)->removeGroup($group_id);
			if ($user->getGroups() == NULL) {$this->delete($id);
			} else {
				$this->save($user);
			}
		}

	}
}

?>
