<?php
	
	namespace Compta\DAO;
	
	use Compta\Domain\Group;
	
	class GroupDAO extends DAO {
		
		/**
		 * Returns an instance of Group.
		 *
		 * @param int|string A group id as an int, or a group name as a string.
		 *
		 * @return object An instance of Group.
		 */
		public function get($info) {
			return $this->getObject($info, 'groups', 'Compta\Domain\Group');
		}
		
		/**
		 * Returns the full list of Group instances, or false if there is none.
		 *
		 * @return Group[]|bool A list of Group instances, as objects, or false is
		 *                      there is no group in database.
		 */
		public function listAll() {
			$query = $this->getDb()->createQueryBuilder();
			$query->select('*')
			      ->from('groups');
			$statement = $query->execute();
			$statement->setFetchMode(\PDO::FETCH_CLASS, 'Compta\Domain\Group');
			$group = $statement->fetchAll();
			if (!$group) return false;
			return $group;
		}
		
		/**
		 * Create a new Group in database, or update an existing one.
		 *
		 * $param Group An instance of Group.
		 */
		public function save(Group $group) {
			$data = array(
				'name' => $group->getName()
			);
			$id = $group->getId();
			if ( $id != NULL)
				$this->getDb()->update('groups', $data, array('id' => $id));
			else {
				$this->getDb()->insert('groups', $data);
				$id = $this->getDb()->lastInsertId();
				$group->setId($id);
			}
		}
		
		/**
		 * Delete a Group instance from database.
		 *
		 * $param int The id of the group to delete.
		 */
		public function delete($id) {
			$this->getDb()->delete('groups', array('id' => $id));
		}
		
	}
	
?>