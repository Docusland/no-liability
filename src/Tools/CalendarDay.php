<?php
namespace Compta\Tools;

/**
 *
 */
class CalendarDay {
	private $day;
	private $present, $debts, $montant;
	function __construct($day) {
		$this->day            = $day;
		$this->present        = [];
		$this->depenses       = [];
		$this->montantCourses = 0;
		$this->dailyCost      = 0;
		$this->debts          = [];
	}
	public function __toString() {
		return "CalendarDay{day:".$this->day.", cost:".$this->dailyCost.", montant:".$this->montantCourses."} ";
	}
	public function getDay() {return $this->day;}
	public function getDateFrenchFormat() {

		return Utilities::toFrenchFormat($this->day);
	}
	public function getKey(){ return "D_".$this->day;}
	public function getPresent() {return $this->present;}
	public function getDepenses() {return $this->depenses;}
	private function cmp($debtA, $debtB) {
		return strcmp($debtA->getUserId(), $debtB->getUserId());
	}
	public function getDebts() {
		usort($this->debts, function ($a, $b) {
				return strcmp($a->getDepenseId(), $b->getDepenseId());
			});
		return $this->debts;
	}
	public function getMontant() {return $this->montantCourses;}
	public function getCost() {return $this->dailyCost;}
	public function setPresent($arr) {
		$this->present = array_merge($this->present, $arr);
	}
	public function addMontant($montant) {
		$this->montantCourses += $montant;
	}

	/**
	 */
	public function addDepense($depense) {

		array_push($this->depenses, $depense);
	}
	public function setDailyCost($cost) {
		$this->dailyCost += $cost;
	}
	public function getIndividualCost() {
		return $this->dailyCost/count($this->present);
	}
	public function setDebts($costToSplit, $users, $depense = null) {
		if ($depense == null) {
			$depenseId                             = 287;
		} elseif ($depense === 290) {$depenseId = $depense;} else {

			$depenseId = $depense->getId();
		}
		$nbUsers = count($users);
		for($i=0;$i<count($users);$i++) {
			$user = $users[$i];
			$cd = new CalendarDebt($this->getDay(), $costToSplit/$nbUsers, $user->getId(), $depenseId);
			array_push($this->debts, $cd);
		}
	}
}
