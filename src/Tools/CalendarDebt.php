<?php
namespace Compta\Tools;
/**
 * Debt owned by a user
 */
class CalendarDebt {
	private $cost;
	private $date;
	private $user_id;
	function __construct($date, $cost, $userId, $depenseId) {
		$this->date      = $date;
		$this->cost      = $cost;
		$this->user_id   = $userId;
		$this->depenseId = $depenseId;
	}

	/**
	 * @return mixed
	 */
	public function getCost() {
		return $this->cost;
	}

	/**
	 * @param mixed $cost
	 *
	 * @return self
	 */
	public function setCost($cost) {
		$this->cost = $cost;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param mixed $date
	 *
	 * @return self
	 */
	public function setDate($date) {
		$this->date = $date;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->user_id;
	}

	/**
	 * @param mixed $user_id
	 *
	 * @return self
	 */
	public function setUserId($user_id) {
		$this->user_id = $user_id;

		return $this;
	}

	/**
	 *  @return mixed
	 */
	public function getDepenseId() {
		return $this->depenseId;
	}

	/**
	 * @param mixed $user_id
	 *
	 * @return self
	 */
	public function setDepenseId($depenseId) {
		$this->depenseId = $depenseId;

		return $this;
	}
}
?>