<?php
namespace Compta\Tools;

/**
 *
 */
class StatisticBoard
{
    private $users;
    private $userFeeds = [];
    private $depenses;
    private $stats                 = [];
    private $nbMeals               = 0;// sum : nb persons per day
    private $averageUserCostPerDay = 0;
    private $totalPaid             = 0;
    private $startDate             = 0;
    private $endDate               = 0;
    private $calendar              = [];

    public function __construct($users, $depenses, $mode)
    {

        /**
         * Sets user Ids as keys.
         */
        $this->users = [];
        foreach ($users as $user) {
            $this->users[$user->getId()] = $user;
        }
        /**
         * Sets depenses Ids as keys.
         */
        $this->depenses = [];
        foreach ($depenses as $depense) {
            $this->depenses[$depense->getId()] = $depense;
        }

        $this->generateStatistics($mode);
    }
    public function __toString()
    {
        return join(",", array_keys($this->stats));
    }
    public function setUsers($users)
    {
        $this->users = $users;
    }
    public function getUsers()
    {
        return $this->users;
    }
    public function setDepenses($depenses)
    {
        $this->depenses = $depenses;
    }
    public function getDepenses()
    {
        return $this->depenses;
    }
    public function getCalendar()
    {
        return array_reverse($this->calendar,true);
    }
    public function getTotalPaid()
    {
        return $this->totalPaid;
    }
    public function getNbMeals()
    {
        return $this->nbMeals;
    }
    /**
     * Get minimum value for given field. The corresponding getter is required.
     */
    private function getMinDate($array, $key)
    {
        $date   = date('Ymd');
        $method = 'get'.ucfirst($key);
        foreach ($array as $record) {
            $recordDate = $record->$method();
            if ($date > $recordDate && $recordDate > 0) {
                $date = $recordDate;
            }
        }
        return $date;
    }
    private function getMaxDate($array, $key)
    {
        $date   = "0";
        $method = 'get'.ucfirst($key);
        foreach ($array as $record) {
            $recordDate = $record->$method();
            if ($date < $recordDate) {
                $date = $recordDate;
            }
        }
        return $date;
    }
    public function getStartDate()
    {
        return $this->startDate;
    }
    public function getEndDate()
    {
        return $this->endDate;
    }
    public function getUserStatistics()
    {
        return $this->stats;
    }
    public function getAvg()
    {
        if ($this->nbMeals > 0) {
            $this->averageUserCostPerDay = round($this->totalPaid/$this->nbMeals);
        }
        // $this->averageUserCostPerDay = 4.5;
        return $this->averageUserCostPerDay;
    }

    /**
     * People present on given day
     * @param day : day given in format YYYYMMDD
     * @param meatEater : if null, no filter, otherwise filters on meat eater or not
     */

    private function usersPresent($day, $isVegetarian = 2)
    {
        $res = [];
        foreach ($this->users as $user) {
            //Strictly inferior to end date. As they are leaving, they should not pay for futur meals

            $isPresent = $user->isPresent($day);
            if($isPresent){
              if ($isVegetarian === 2) {
                array_push($res, $user);
              } else {
                if ($isVegetarian and $user->getIsVegetarian()) {
                  array_push($res, $user);
                } elseif (!$isVegetarian and !$user->getIsVegetarian()) {
                  array_push($res, $user);
                }
              }
            }
            // $usdate = $user->getStartDate();
            // $uedate = $user->getEndDate();
            // if ((intval($usdate) <= intval($day)) &&
            //     (intval($day) < intval($uedate))) {
            // }
        }

        return $res;
    }

    /**
     * Function that parses users and depenses for defining event's first and last date.
     */
    private function initStats()
    {
        $user_min_date = $this->getMinDate($this->users, "startDate");
        $user_max_date = $this->getMaxDate($this->users, "endDate");
        $dep_min_date  = $this->getMinDate($this->depenses, "date");
        $dep_max_date  = $this->getMaxDate($this->depenses, "date");

        $dep_max_date = (int) $dep_max_date +1;
        $dep_max_date = (string) $dep_max_date;
        $this->startDate = min($user_min_date, $dep_min_date);
        $this->endDate   = max($user_max_date, $dep_max_date);
    }
    /**
     * Function which generates the calendar array
     */
    private function initCalendar()
    {
        $begin    = date_create_from_format("Ymd", $this->startDate);
        $end      = date_create_from_format("Ymd", $this->endDate);
        $interval = \DateInterval::createFromDateString('1 day');
        $period   = new \DatePeriod($begin, $interval, $end);

        foreach ($period as $dt) {
            $key          = $dt->format("Ymd");
            $calDay       = new CalendarDay($key);
            $user_present = $this->usersPresent($key);
            // $calDay->setPresent($user_present);
            // error_log("Found ".count($user_present)." people on $key");
            $this->calendar["D_".$key] = $calDay;
        }
        error_log("--> Total meals to give =".$this->nbMeals);
    }

    /**
     * Loads depenses in calendar.
     * Defines totalPaid
     * Defines in calendarDar : depense, montant
     */
    private function loadDepensesInCalendar()
    {
        foreach ($this->getDepenses() as $depense) {
            $day = $depense->getDate();
            $key = "D_".$day;
            $this->calendar[$key]->addDepense($depense);
            $this->calendar[$key]->addMontant($depense->getMontant());
            $this->totalPaid += $depense->getMontant();
        }
    }

    /**
    * Calculates the remaining days in the current event
    */
    // TOFIX for several periods management.
    private function getRemainingNights($calDay)
    {
        $i = array_search($calDay->getKey(), array_keys($this->calendar));
        return count($this->calendar)-$i-1;
    }
    /**
     * Parses tha calendar and generates a smart repartition of debts.
     * Depending on the average daily cost split the :
     * nbdays =	cost's (expense note) / average day cost
     * spread it proportionally in the nbdays for each user concerned
     */
    public function generateSmartFeeds()
    {
        $presences = [];
        foreach ($this->calendar as $key => $calDay) {
            $user_present = $this->usersPresent($calDay->getDay());

            $nbExpenses = count($calDay->getDepenses()) ;
            foreach ($calDay->getDepenses() as $depense) {
                $montant = $depense->getMontant();
                $nbNights = $depense->getNbNights();
								$users_concerned = $depense->getUsers();
								$users = $this->intersectArray($user_present, $users_concerned);
                if ($montant > 0) {
                    $remainingNights = $this->getRemainingNights($calDay);
                    if ($nbNights==0) {
                        // Generate the nbNights automatically


                        $d   = date_create_from_format("Ymd", $calDay->getDay());

                        // Defining a default DAILY COST of 8
                        $default_daily_bill  = 8/$nbExpenses;
                        $nbLoops=0;
                        while ($nbLoops<$remainingNights &&$montant > ($default_daily_bill*count($user_present))) {
                            $d  = date_add($d, date_interval_create_from_date_string("1 days"));
                            $key = $d->format("Ymd");
                            $tempDay = $this->calendar["D_".$key];
                            $user_temp_present = $this->usersPresent($tempDay->getDay());

		                        $users = $this->intersectArray($user_temp_present,  $depense->getUsers());
                            $nbLoops++;
                            $montant -= $default_daily_bill;
                        }

                        // Defining a default MAXIMUM DURATION EXPENSE of 5
                        $nbNights = min($nbLoops, $remainingNights, 5);
                        $nbNights = max($nbNights, 1);
                    } else {
                        $nbNights = min($nbNights, $remainingNights);
                    } // Never trust the user
                    $montant = $depense->getMontant();
                    $costPerDay = round($montant/$nbNights);
                    for ($i = 0; $i < $nbNights; $i++) {
                        $d   = date_create_from_format("Ymd", $calDay->getDay());
                        $da  = date_add($d, date_interval_create_from_date_string($i." days"));
                        $key = $da->format("Ymd");
                        $tempDay = $this->calendar["D_".$key];
                        $cost    = $costPerDay;
                        if ($montant < $costPerDay) {
                            $cost = $montant;
                        }

                        $param = 2;
                        if (!$depense->getIsVegetarian()) {
                            $param = $depense->getIsVegetarian();
                        }
												$user_temp = $this->usersPresent($tempDay->getDay());
												$users = $this->intersectArray($user_temp, $users_concerned);
                        $tempDay->setDebts($cost, $users, $depense);



                    }
                }

            }
        }
    }
    public function intersectArray($a, $b)
    {
        $res = [];
        if ($a != null && $b != null) {
            foreach ($a as $keyA => $valueA) {
                if ($valueA) {
										$left_key = $valueA;
										if(!is_string($valueA)){
											switch((get_class($valueA))){
												case 'Compta\Domain\User':
													$left_key = (string) $valueA->getId();
													break;
												default:
													error_log("Unmanaged class in intersect array A ".get_class($valueA));
													break;
											}
										}

                    foreach ($b as $keyB => $valueB) {
                        $areSame=false;
                        if ($valueB) {
													$right_key = $valueB;
													if(!is_string($valueB)){
														switch((get_class($valueA))){
															case 'Compta\Domain\User':
																$right_key = (string) $valueB->getId();
																break;
															default:
																error_log("Unmanaged class in intersect array B ".get_class($valueA));
																break;
														}
													}
													$areSame = ($left_key == $right_key);
													if ($areSame) {
	                            $res[]=$valueA;
	                        }
                        }



                    }
                }
            }

						return $res;
        }
			}




    /**
     * Spreads a daily cost for each present on the given day.
     * Vegetarians pay a calculated cost.
     */
    public function generateParitarianFeeds()
    {

        // Vegetarian daily cost =
        // sum depense from first day where non vegetarian meal is defined to last day a non vegetarian meal is defined => calculate percentage

        usort($this->depenses, function ($a, $b) {
            return strcmp($a->getDate(), $b->getDate());
        });
        $beg     = "99999999";
        $end     = "0";
        $totalP  = 0;
        $totalPV = 0;
        foreach ($this->depenses as $key => $depense) {
            // if (!$depense->getIsVegetarian()) {
                if ($depense->getDate() > $end) {
                    $end = $depense->getDate();
                }

                if ($depense->getDate() < $beg) {
                    $beg = $depense->getDate();
                }
            // }
        }
        foreach ($this->depenses as $key => $depense) {
            if ($depense->getDate() <= $end && $depense->getDate() >= $beg) {
                $totalP += $depense->getMontant();
                if ($depense->getIsVegetarian()) {
                    $totalPV += $depense->getMontant();
                }
            }
        }

        $nbVegeMeals = 0;
        foreach ($this->users as $key => $user) {
            if ($user->getIsVegetarian()) {
                // foreach ($user->getDureeSejour() as $key => $duree) {
                // 	$nbVegeMeals += $duree;
                // }
            }
        }

        // error_log("Vege % : ".$totalPV/$totalP);
        error_log("--> Calculated between : $beg ->$end %= $totalPV / $totalP");
        $dailyCost = round($this->totalPaid/$this->nbMeals, 2);
        error_log("Daily meat cost : $dailyCost");
        $dailyVege = round(($totalPV/$totalP)*$dailyCost);
        error_log("Daily vege cost : $dailyVege");
        $dailyMeatEater = round(($this->totalPaid-$dailyVege*$nbVegeMeals)/($this->nbMeals-$nbVegeMeals), 2);

        foreach ($this->calendar as $key => $calDay) {
            $beefEaters = $this->usersPresent($calDay->getDay(), false);
            $calDay->setDebts($dailyMeatEater*count($beefEaters), $beefEaters);
            $grassEaters = $this->usersPresent($calDay->getDay(), true);
            $calDay->setDebts($dailyVege*count($grassEaters), $grassEaters, 290);
        }
    }
    public function getFinalStatistics()
    {
        $result = [];
        foreach ($this->users as $user) {
            $result[$user->getId()] = [$debt = 0, $paid = 0];
        }
        foreach ($this->calendar as $calendarDay) {
            foreach ($calendarDay->getDebts() as $debt) {
                $result[$debt->getUserId()][0] += $debt->getCost();
            }
            foreach ($calendarDay->getDepenses() as $depense) {
                $result[$depense->getUserId()][1] += $depense->getMontant();
            }
        }
        return $result;
    }
    public function generateStatistics($mode)
    {
        $this->initStats();
        $this->initCalendar();
        $this->loadDepensesInCalendar();
        switch ($mode) {
            case 'paritarian':
                $this->generateParitarianFeeds();
                break;
            case "time":
                $this->generateSmartFeeds();
                break;
            default:
                break;
        }
    }
}
