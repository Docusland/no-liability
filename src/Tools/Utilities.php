<?php
namespace Compta\Tools;

/**
 *
 */
class Utilities
{
  public static function toFrenchFormat($day){
    $date = date_create_from_format("Ymd", $day);
		return $date->format('d/m/Y');
  }
}
