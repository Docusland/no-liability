<?php
	
	namespace Compta\Controller;
	
	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;
	
	trait Logging {
		
		/**
		 * Write down a JSON string in the logs
		 *
		 * @param string      $json The JSON string to log.
		 * @param Application $app  The running application.
		 */
		protected function logJSON($json, Application $app) {
			if ($app['monolog.level'] == 'debug') {
				$date = strftime('[%F %T] ', time());
				$prefix = 'Compta.JSON.out: ';
				$json = array_reverse(explode("\n", $json))[0];
				$file = fopen($app['monolog.logfile'], 'a');
				fwrite($file, $date.$prefix.$json."\n");
				fclose($file);
			}
		}
		
	}
	
?>
