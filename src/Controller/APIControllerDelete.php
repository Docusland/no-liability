<?php

	namespace Compta\Controller;

	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;

	class APIControllerDelete {

		use Security;
		use Logging;

		/**
		 * Delete a depense
		 *
		 * @param int         $id      The id of the depense to delete.
		 * @param Request     $request The request sent to the application, should
		 *                             include a 'apikey' HTTP header.
		 * @param Application $app     The running application.
		 *
		 * @return string Returns a JSON string stating if the operation has been a
		 *                success, or that no depense has been found the given id.
		 */
		public function deleteDepense($id, Request $request, Application $app) {
			$json = $this->isJSONLoggedIn($request, $app);
			if ($json === NULL) {
				if ($app['dao.depense']->get($id)) {
					$app['dao.depense']->delete($id);
					$json = $app->json(array(
						'status' => 'OK'
					), 200);
				}
				else {
					$json = $app->json(array(
						'status' => 'KO',
						'error' => 'Pas de dépense enregistrée avec l’id '.$id
					), 400);
				}
			}
			$this->logJSON($json, $app);
			return $json;
		}

		/**
		 * Delete an user, and all depenses without any referring user
		 *
		 * @param int         $id      The id of the user to delete.
		 * @param Request     $request The request sent to the application, should
		 *                             include a 'apikey' HTTP header.
		 * @param Application $app     The running application.
		 *
		 * @return string Returns a JSON string stating if the operation has been a
		 *                success, or that no user has been found the given id.
		 */
		public function deleteUser($id, Request $request, Application $app) {
			$json = $this->isJSONLoggedIn($request, $app);
			if ($json === NULL) {
				if ($app['dao.user']->get($id)) {
					$app['dao.depense']->deleteByUser($id);
					$app['dao.user']->delete($id);
					$json = $app->json(array(
						'status' => 'OK'
					), 200);
				}
				else {
					$json = $app->json(array(
						'status' => 'KO',
						'error' => 'Pas d’utilisateur enregistré avec l’id '.$id
					), 400);
				}
			}
			$this->logJSON($json, $app);
			return $json;
		}

		/**
		 * Delete a group, all users without any referring group, and all depenses
		 * without any referring group or user
		 *
		 * @param int         $id      The id of the group to delete.
		 * @param Request     $request The request sent to the application, should
		 *                             include a 'apikey' HTTP header.
		 * @param Application $app     The running application.
		 *
		 * @return string Returns a JSON string stating if the operation has been a
		 *                success, or that no group has been found the given id.
		 */
		public function deleteGroup($id, Request $request, Application $app) {
			$json = $this->isJSONLoggedIn($request, $app);
			if ($json === NULL) {
				if ($app['dao.group']->get($id)) {
					$app['dao.depense']->deleteByGroup($id);
					$app['dao.user']->removeFromGroup($id);
					$app['dao.group']->delete($id);
					$json = $app->json(array(
						'status' => 'OK'
					), 200);
				}
				else {
					$json = $app->json(array(
						'status' => 'KO',
						'error' => 'Pas de groupe enregistré avec l’id '.$id
					), 400);
				}
			}
			$this->logJSON($json, $app);
			return $json;
		}

	}

?>
