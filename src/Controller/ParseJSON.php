<?php
	
	namespace Compta\Controller;
	
	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;
	
	trait ParseJSON {
		
		/**
		 * Check that all fields needed in an input JSON file are present.
		 *
		 * @param string[]    $params  The list of required fields in the input JSON
		 *                             file, as strings.
		 * @param Request     $request The request sent to the application.
		 * @param Application $app     The running application.
		 * 
		 * @return string|null Returns null if no field is missing, or a JSON string
		 *                     giving the first missing field name.
		 */
		protected function missingParameter(
			array $params,
			Request $request,
			Application $app
		) {
			foreach ($params as $param)
				if (!$request->request->has($param))
					return $app->json(array(
						'status' => 'KO',
						'error' => 'Paramètre requis manquant : '.$param
					), 400);
			return NULL;
		}
		
	}
	
?>
