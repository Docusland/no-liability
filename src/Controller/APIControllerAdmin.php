<?php

namespace Compta\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class APIControllerAdmin {

	use ParseJSON;
	use Security;
	use Logging;

	/**
	 * Return an API key to use for admin actions.
	 *
	 * @param Request     $request The request sent to the application, should
	 *                             include a JSON string with the 'name' and
	 *                             'password' fields.
	 * @param Application $app     The running application.
	 *
	 * @return string Returns a JSON string including an API key if the login
	 *                and password given in the request are valid, or a JSON
	 *                string with an error message if one or the other is not
	 *                valid.
	 */
	public function login(Request $request, Application $app) {
		$params = ['name', 'password'];
		$json   = $this->missingParameter($params, $request, $app);
		if ($json === NULL) {
			$login    = $request->request->get('name');
			$password = $request->request->get('password');
			if (
				!isset($app['admin'][$login]) ||
				$app['admin'][$login] != $password
			) {
				$json = $app->json(array(
						'status' => 'KO',
						'error'  => 'Mot de passe incorrect pour l’utilisateur '.$login,
					), 400);
			} else {
				$key           = base64_encode(random_bytes(64));
				$keyexpiration = time()+$app['keyexpiration'];
				$keylist       = fopen($app['keylist'], 'a');
				fwrite($keylist, $key."\n".$keyexpiration."\n");
				fclose($keylist);
				$json = $app->json(array(
						'key'    => $key,
						'status' => 'OK',
					), 200);
			}
		}
		$this->logJSON($json, $app);
		$app['monolog']->debug("EDUEDUEUDEU");
		// $app['monolog']->debug($json);
		return $json;
	}

	/**
	 * Revoke an API key.
	 *
	 * @param Request     $request The request sent to the application, should
	 *                             include a 'apikey' HTTP header.
	 * @param Application $app     The running application.
	 *
	 * @return string Returns a JSON string.
	 */
	public function logout(Request $request, Application $app) {
		$json = $this->isJSONLoggedIn($request, $app);
		if ($json === NULL) {
			$key     = $request->headers->get('apikey');
			$keylist = $this->getKeylist($app);
			$length  = count($keylist);
			$file    = fopen($app['keylist'], 'w');
			for ($i = 0; $i < $length; $i += 2) {
				if ($keylist[$i] != $key) {
					fwrite($file, $keylist[$i]."\n".$keylist[($i+1)]."\n");
				}
			}

			fclose($file);
			$json = $app->json(array(
					'status' => 'OK',
				), 200);
		}
		$this->logJSON($json, $app);
		return $json;
	}

}

?>
