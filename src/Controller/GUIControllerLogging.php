<?php
namespace Compta\Controller;

use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class GUIControllerLogging {
	use Security;

	/**
	 * Connection form for admin actions.
	 *
	 * @param Request     $request The request sent to the application, should
	 *                             include a JSON string with the 'name' and
	 *                             'password' fields.
	 * @param Application $app     The running application.
	 *
	 * @redirects to homePage.
	 */
	public function login(Request $request, Application $app) {
		// some default data for when the form is displayed the first time
		$data = array(
			'name'     => '',
			'password' => '',
		);
		$form = $app['form.factory']->createBuilder(FormType::class , $data)
		                            ->add('name')
		                            ->add('password', PasswordType::class )
		                            ->add('submit', SubmitType::class , [
				'label' => 'Login',
			])
			->getForm();

		$form->handleRequest($request);

		if ($form->isValid()) {
			$data       = $form->getData();
			$subRequest = Request::create('/api/login', 'POST', array('name' => $data['name'], 'password' => $data['password']));

			$response = $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST, false);
			$content  = $response->getContent();
			$result   = json_decode($content);
			if ($result->{'status'} == 'OK') {
				$key = $result->{'key'};
				$app['monolog']->debug("LOGGED IN key=".$key);
				$app['session']->set('session_key', $key);
				//$request->headers->set('apikey', $key);
				return $app->redirect('/');
			} else {
				$app['monolog']->error($result->{'error'});
				return $app->redirect('/connect');
			}
		} else {
			$data   = $form->getData();
			$errors = $form->getErrors(true);
			$app['monolog']->debug("ERRORS : ");
			foreach ($errors as $key => $err) {
				$app['monolog']->error($err);
			}
		}
		// display the form
		return $app['twig']->render('login.twig', array(
				'form'    => $form->createView(),
				'session' => $app["session"]));
	}

	/**
	 * Disconnection form for admins.
	 *
	 * @param Request     $request The request sent to the application, should
	 *                             include a JSON string with the 'name' and
	 *                             'password' fields.
	 * @param Application $app     The running application.
	 *
	 * @redirects to login Page.
	 */
	public function logout(Request $request, Application $app) {

		$data = array(
			'HTTP_apikey' => $app['session']->get('session_key')
		);
		$subRequest = Request::create('/api/logout', 'GET', $data);
		$subRequest->headers->set('apikey', $app['session']->get('session_key'));
		$response = $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST, false);

		$content = $response->getContent();
		$result  = json_decode($content);
		if ($result->{'status'} == 'OK') {
			$app['session']->remove('session_key');
			$app['monolog']->debug("LOGGED OUT key=".$data['HTTP_apikey']);
		} else {
			$app['monolog']->error($result->{'error'});
		}
		return $app->redirect('/');
	}

}
?>
