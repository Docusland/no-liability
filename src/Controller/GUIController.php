<?php
namespace Compta\Controller;
use Compta\Domain\Depense;

use Compta\Tools\StatisticBoard;
use Silex\Application;

use Symfony\Component\HttpFoundation\Request;
const DEFAULT_GROUP = 1;

class GUIController {
	use Security;

	/**
	 * Get home page.
	 *
	 * @param Request     $request The request sent to the application, should
	 *                             include a JSON string with the 'name' and
	 *                             'password' fields.
	 * @param Application $app     The running application.
	 *
	 * @returns the corresponing page.
	 */
	public function getHomePage(Request $request, Application $app) {
		if ($this->isLoggedIn($request, $app)) {
			$data = $this->getDepensesData($app);
			// print(json_encode($data));
			return $app['twig']->render('home.twig', $data);
		}
		return $app->redirect('/connect');
	}

	/**
	 * Edit a specified depense.
	 */
	public function editDepense($id, Request $request, Application $app) {
		$app['monolog']->debug("Coucou... ".$id);

		//TODO : move this one line below when better user management.
		$data = $this->getDepensesData($app);
		if ($this->isLoggedIn($request, $app)) {
			$depense = $app['dao.depense']->get($id);
			$options = ["users" => $data['users']];
			$form    = $app['form.factory']->create("Compta\Form\DepenseType", $depense, $options);
			$form->handleRequest($request);

			$app['monolog']->debug("Saving... ".$depense);
			if ($form->isSubmitted() && $form->isValid()) {

				$depense = $form->getData();
				$depense->setGroupId(DEFAULT_GROUP);

				//TODO : Find a better way for concernes management.
				$concernes     = $depense->getUsers();
				$concernesList = [];
				foreach ($concernes as $concerne) {
					array_push($concernesList, $concerne->getId());
				}
				$depense->setUsers($concernesList);

				$app['dao.depense']->save($depense);
			} else {
				$app['monolog']->debug("Encountered an issue");
				foreach ($form->getErrors() as $error) {
					$app['monolog']->error($error->getMessage());
					$request->getSession()->getFlashBag()->add('error', $error->getMessage());
				};
			}

			$app['monolog']->debug("Saved : ".$depense);
			$request->getSession()->getFlashBag()->add('success', 'La créance a été mise à jour.');
		}
		return $app['twig']->render('depenses.twig', array(
				"depenses"    => $data['depenses'],
				"users"       => $data['users'],
				"form"        => $form->createView(),
				"form_delete" => $form->createView(),
				"edit"        => true,
				"add"					=> false,
				"delete"			=> false,
				"userIsLogged" => $this->isLoggedIn($request, $app)
			));
	}

	/**
	 * Delete a specified depense.
	 */
	public function deleteDepense($id, Request $request, Application $app) {
		// TOFIX
		$app['monolog']->debug("Deleting Depense... ".$id);

		//TODO : move this one line below when better user management.
		$data = $this->getDepensesData($app);
		if ($this->isLoggedIn($request, $app)) {
			$depense = $app['dao.depense']->get($id);
			// $options = ["users" => $data['users']];
			 $form    = $app['form.factory']->create("Compta\Form\DeleteDepenseType", $depense);
			 $form->handleRequest($request);
			// $app['dao.depense']->delete($depense->getId());
			 $app['monolog']->debug("Deleting... ".$depense);
			 if ($form->isSubmitted() && $form->isValid()) {
				$app['dao.depense']->delete($depense->getId());
				return $app->redirect('/depenses');
				} else {
					$app['monolog']->debug("Encountered an issue");
					foreach ($form->getErrors() as $error) {
						$app['monolog']->error($error->getMessage());
						$request->getSession()->getFlashBag()->add('error', $error->getMessage());
					};
				}

			$app['monolog']->debug("Deleted : ".$depense);
			$request->getSession()->getFlashBag()->add('success', 'La créance a été supprimée.');
		}
		return $app['twig']->render('depenses.twig', array(
				"depenses"    => $data['depenses'],
				"users"       => $data['users'],
				"form"        => $form->createView(),
				"form_delete" => $form->createView(),
				"edit"        => false,
				"add"					=> false,
				"delete"			=> true,
				"userIsLogged" => $this->isLoggedIn($request, $app)
			));
	}



	public function getDepensesPage(Request $request, Application $app) {
		//TODO : move this one line below when better user management.
		$data = $this->getDepensesData($app);
		if ($this->isLoggedIn($request, $app)) {
			$depense = new Depense;
			$options = ["users" => $data['users']];
			$form    = $app['form.factory']->create("Compta\Form\DepenseType", $depense, $options);

			$form->handleRequest($request);

			$app['monolog']->debug("Saving... ".$depense);

			if ($form->isValid()) {
				$depense = $form->getData();
				$depense->setGroupId(DEFAULT_GROUP);

				//TODO : Find a better way for concernes management.
				$concernes     = $depense->getUsers();
				$concernesList = [];
				foreach ($concernes as $concerne) {
					array_push($concernesList, $concerne->getId());
				}
				$depense->setUsers($concernesList);

				$app['dao.depense']->save($depense);
				$app['monolog']->debug("Saved : ".$depense);
				$request->getSession()->getFlashBag()->add('success', 'La créance a été sauvegardée.');

			} else {
				$app['monolog']->debug("Encountered an issue");
				foreach ($form->getErrors() as $error) {
					$app['monolog']->error($error->getMessage());
					$request->getSession()->getFlashBag()->add('error', $error->getMessage());
				};
			}

			$delete_form = $app['form.factory']->create("Compta\Form\DeleteDepenseType", $depense);

			$app['monolog']->debug("Checking the delete form");
			if ($delete_form->isValid()) {
				$app['monolog']->debug("Delete form is valid");

				$data = $delete_form->getData();
				$app['monolog']->debug("Deleting record... ".$data["id"]);
				$depenses = $app['dao.depense']->delete($data["id"]);
				$request->getSession()->getFlashBag()->add('success', 'La créance a été supprimée.');
			} else {
				foreach ($delete_form->getData() as $key => $value) {
					$app['monolog']->error("Invalid values $key -> $value");
				}
			}

			return $app['twig']->render('depenses.twig', array(
					"depenses"    => $data['depenses'],
					"users"       => $data['users'],
					"form"        => $form->createView(),
					"form_delete" => $delete_form->createView(),
					"edit"        => false,
					"add"					=> true,
					"delete"			=> false,
					"userIsLogged" => $this->isLoggedIn($request, $app)
					));
		} else {
			return $app->redirect('/connect');
		}

	}
	private function getDepensesData($app) {
		$result             = [];
		$result['depenses'] = $app['dao.depense']->findByGroup(DEFAULT_GROUP);
		$result['users']    = [];
		$users              = $app['dao.user']->findByGroup(DEFAULT_GROUP);
		foreach ($users as $user) {
			$result['users'][$user->getid()] = $user;
		}

		return $result;
	}
	/**
	 * Get statistics of users and expenses.
	 *
	 * @param Request     $request The request sent to the application, should
	 *                             include a JSON string with the 'name' and
	 *                             'password' fields.
	 * @param Application $app     The running application.
	 *
	 * @returns the statistics page.
	 */
	public function getStatisticsPage(Request $request, Application $app) {
		$users    = $app['dao.user']->findByGroup(DEFAULT_GROUP);
		$depenses = $app['dao.depense']->findByGroup(DEFAULT_GROUP);
		$stat     = new StatisticBoard($users, $depenses, "time");
		return $app['twig']->render('statistics.twig', array(
				"stats" => $stat,
			));
	}
	public function getParitarianStatisticsPage(Request $request, Application $app) {
		$users    = $app['dao.user']->findByGroup(DEFAULT_GROUP);
		$depenses = $app['dao.depense']->findByGroup(DEFAULT_GROUP);
		$stat     = new StatisticBoard($users, $depenses, "paritarian");
		return $app['twig']->render('paritarian_stats.twig', array(
				"stats" => $stat,
			));
	}

	/**
	 * Get all users.
	 *
	 * @param Request     $request The request sent to the application, should
	 *                             include a JSON string with the 'name' and
	 *                             'password' fields.
	 * @param Application $app     The running application.
	 *
	 * @returns an array of users.
	 */
	public function getUsers(Request $request, Application $app) {

		$users = $app['dao.user']->findAll();
		return $app['twig']->render('users.twig', array(
				"users" => $users,
				"mode" => null,
			));
	}

	/**
	 * Edit a user.
	 */
	public function editUser($id, Request $request, Application $app) {
		$app['monolog']->debug("Coucou... User".$id);
		$users = $app['dao.user']->findAll();
		$options = ["periods" => []];
		$formView=null;

		if ($this->isLoggedIn($request, $app)) {
			$user = $app['dao.user']->get($id);
			$form    = $app['form.factory']->create("Compta\Form\UserType", $user, $options);
			$form->handleRequest($request);
			$app['monolog']->debug("Saving... ".$user);
			$formView=$form->createView();
			if ($form->isSubmitted() && $form->isValid()) {

				$depense = $form->getData();
				$depense->setGroups([DEFAULT_GROUP]);

				$app['dao.user']->save($user);
				$app['monolog']->debug("Saved : ".$user);
				$request->getSession()->getFlashBag()->add('success', 'L\'utilisateur a été mis à jour.');
				return $app->redirect('/users');
			} else {
				$app['monolog']->debug("Encountered an issue");
				foreach ($form->getErrors() as $error) {
					$app['monolog']->error($error->getMessage());
					$request->getSession()->getFlashBag()->add('error', $error->getMessage());
				};
			}


		}else{
			 return $app->redirect('/');
		}

		return $app['twig']->render('users.twig', array(
				"users"       => $users,
				"form" => $formView,
				"mode" => "edit",
				"userIsLogged" => $this->isLoggedIn($request, $app)
			));
	}

	/**
	 * Delete a specified user.
	 */
	public function deleteUser($id, Request $request, Application $app) {
		$app['monolog']->debug("Deleting... User $id");
		$users = $app['dao.user']->findAll();
		$options = ["periods" => []];
		$user = $app['dao.user']->get($id);
		$form    = $app['form.factory']->create("Compta\Form\UserType", $user, $options);
		$form->handleRequest($request);

		if ($this->isLoggedIn($request, $app)) {
			 if ($form->isSubmitted() && $form->isValid()) {
				 $app['monolog']->debug("Deleting...User $user");
				$app['dao.user']->delete($user->getId());
				return $app->redirect('/users');
				} else {
					foreach ($form->getErrors() as $error) {
						$app['monolog']->error($error->getMessage());
						$request->getSession()->getFlashBag()->add('error', $error->getMessage());
					};
				}

			$app['monolog']->debug("Deleted : ".$user);
			$request->getSession()->getFlashBag()->add('success', 'L\'utilisateur a été supprimé');


		return $app['twig']->render('users.twig', array(
				"users"       => $users,
				"form" => $form->createView(),
				"mode" => "delete",
				"userIsLogged" => $this->isLoggedIn($request, $app)
			));
	}
}
}
?>
