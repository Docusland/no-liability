<?php
	
	namespace Compta\Controller;
	
	use Silex\Application;
	
	class APIControllerRead {
		
		use Logging;
		
		/**
		 * Display the list of groups.
		 *
		 * @param Application $app The running application.
		 *
		 * @return string Returns a JSON string listing the groups, or an error
		 *                message if no group has been found.
		 */
		public function getGroups(Application $app) {
			$json = NULL;
			$groups = $app['dao.group']->listAll();
			if (!$groups) $json = $app->json(array(
				'status' => 'KO',
				'error' => 'Pas de groupe enregistré'
			), 400);
			if ($json === NULL) {
				$result = [];
				foreach ($groups as $group) {
					$result[] = array(
						'id' => $group->getId(),
						'namegroup' => $group->getName()
					);
				}
				$json = $app->json(array(
					'records' => $result,
					'status' => 'OK'
				), 200);
			}
			$this->logJSON($json, $app);
			return $json;
		}
		
		/**
		 * Display the list of users in the given group.
		 *
		 * @param int         $group_id The id of a group.
		 * @param Application $app      The running application.
		 *
		 * @return string Returns a JSON string listing the users.
		 */
		public function getUsers($group_id, Application $app) {
			$users = $app['dao.user']->findByGroup($group_id);
			if (!empty($users)) {
				$result = [];
				foreach ($users as $user) {
					$groups = implode(',', $user->getGroups());
					$result[] = array(
						'id' => $user->getId(),
						'username' => $user->getName(),
						'usergroup' => $groups,
						'usercolor' => $user->getColor()
					);
				}
				$json = $app->json(array(
					'records' => $result,
					'status' => 'OK'
				), 200);
			}
			else {
				$json = $app->json(array(
					'status' => 'KO',
					'error' => 'Pas d’utilisateur enregistré dans le groupe identifié par l’id '.$group_id
				), 400);
			}
			$this->logJSON($json, $app);
			return $json;
		}
		
		/**
		 * Display the list of all users.
		 *
		 * @param Application $app The running application.
		 *
		 * @return string Returns a JSON string listing all the users.
		 */
		public function getUsersAll(Application $app) {
			$users = $app['dao.user']->findAll();
			if (empty($users)) {
				$json = $app->json(array(
					'status' => 'KO',
					'error' => 'Pas d’utilisateur enregistré'
				), 400);
			}
			else {
				$result = [];
				foreach ($users as $user) {
					$groups = implode(',', $user->getGroups());
					$result[] = array(
						'id' => $user->getId(),
						'username' => $user->getName(),
						'usergroup' => $groups,
						'usercolor' => $user->getColor()
					);
				}
				$json = $app->json(array(
					'records' => $result,
					'status' => 'OK'
				), 200);
			}
			$this->logJSON($json, $app);
			return $json;
		}
		
		/**
		 * Display the list of depenses in the given group.
		 *
		 * @param int         $group_id The id of a group.
		 * @param Application $app      The running application.
		 *
		 * @return string Returns a JSON string listing the depenses.
		 */
		public function getDepenses($group_id, Application $app) {
			$json = NULL;
			$depenses = $app['dao.depense']->findByGroup($group_id);
			if (!$depenses)
				$json = $app->json(array(
					'status' => 'KO',
					'error' => 'Pas de dépense enregistrée dans le groupe identifié par l’id '.$group_id
				), 400);
			if ($json === NULL) {
				foreach ($depenses as $depense) {
					$records[] = array(
						'id' => $depense->getId(),
						'montant' => $depense->getMontant(),
						'payeur' => $depense->getUserId(),
						'concernes' => implode(',', $depense->getUsers()),
						'date' => $depense->getDate(),
						'usergroup' => $depense->getGroupId(),
						'description' => $depense->getName()
					);
				}
				$json = $app->json(array(
					'records' => $records,
					'status' => 'OK'
				), 200);
			}
			$this->logJSON($json, $app);
			return $json;
		}
		
		/**
		 * Display the list of all depenses.
		 *
		 * @param Application $app The running application.
		 *
		 * @return string Returns a JSON string listing all the depenses.
		 */
		public function getDepensesAll(Application $app) {
			$json = NULL;
			$depenses = $app['dao.depense']->findAll();
			if (!$depenses)
				$json = $app->json(array(
					'status' => 'KO',
					'error' => 'Pas de dépense enregistrée'
				), 400);
			if ($json === NULL) {
				foreach ($depenses as $depense) {
					$records[] = array(
						'id' => $depense->getId(),
						'montant' => $depense->getMontant(),
						'payeur' => $depense->getUserId(),
						'concernes' => implode(',', $depense->getUsers()),
						'date' => $depense->getDate(),
						'usergroup' => $depense->getGroupId(),
						'description' => $depense->getName()
					);
				}
				$json = $app->json(array(
					'records' => $records,
					'status' => 'OK'
				), 200);
			}
			$this->logJSON($json, $app);
			return $json;
		}
		
	}
	
?>
