<?php

	namespace Compta\Controller;

	use Silex\Application;
	use Symfony\Component\HttpFoundation\Request;
	use Compta\Domain\Depense;
	use Compta\Domain\Group;
	use Compta\Domain\User;

	class APIControllerCreate {

		use ParseJSON;
		use Security;
		use Logging;

		/**
		 * Check that the field 'usergroup' given in a request reference a known
		 * group.
		 *
		 * @param Request     $request The request sent to the application.
		 * @param Application $app     The running application.
		 *
		 * @return string|null Returns null if the group exists, or a JSON string
		 *                     giving an error message.
		 */
		private function wrongGroup(Request $request, Application $app) {
			$groups = explode(',', $request->request->get('usergroup'));
			foreach ($groups as $group) {
				$usergroup = $app['dao.group']->get($group);
				if (!$usergroup) {
					return $app->json(array(
						'status' => 'KO',
						'error' => 'Pas de groupe enregistré avec l’id '.$group
					), 400);
				}
			}
			return NULL;
		}

		/**
		 * Return a JSON string including a 'records' field and a success creation
		 * status.
		 *
		 * @param string      $result The value to put in the 'result' field of the
		 *                            JSON string, should be a JSON string itself.
		 * @param Application $app    The running application.
		 *
		 * @return string a JSON string including a 'records' field.
		 */
		private function successfulOperation($result, $app) {
			return $app->json(array(
				'records' => $result,
				'status' => 'OK'
			), 201);
		}

		/**
		 * Parse a JSON string and create a group entry in database from it.
		 *
		 * @param Request     $request The request sent to the application.
		 * @param Application $app     The running application.
		 *
		 * @return string A JSON string stating that the operation is a success or
		 *                giving an error message.
		 */
		public function addGroup(Request $request, Application $app) {
			$json = $this->isJSONLoggedIn($request, $app);
			if ($json === NULL ) {
				$params = ['namegroup'];
				$json = $this->missingParameter($params, $request, $app);
			}
			if ($json === NULL ) {
				$group = ($request->request->has('id')) ?
					$app['dao.group']->get($request->request->get('id')) :
					new Group() ;
				if (!$group) {
					$json = $app->json(array(
						'status' => 'KO',
						'error' => 'Pas de groupe enregistré avec l’id '.$request->request->get('id')
					), 400);
				}
				else {
					$group->setName($request->request->get('namegroup'));
					$app['dao.group']->save($group);
					$result = array(
						'id' => $group->getId(),
						'namegroup' => $group->getName()
					);
					$json = $this->successfulOperation($result, $app);
				}
			}
			$this->logJSON($json, $app);
			return $json;
		}

		/**
		 * Parse a JSON string and create a group user in database from it.
		 *
		 * @param Request     $request The request sent to the application.
		 * @param Application $app     The running application.
		 *
		 * @return string A JSON string stating that the operation is a success or
		 *                giving an error message.
		 */
		public function addUser(Request $request, Application $app) {
			$json = $this->isJSONLoggedIn($request, $app);
			if ($json === NULL ) {
				$params = ['username', 'usercolor', 'usergroup'];
				$json = $this->missingParameter($params, $request, $app);
			}
			if ($json === NULL ) $json = $this->wrongGroup($request, $app);
			if ($json === NULL ) {
				$user = ($request->request->has('id')) ?
					$app['dao.user']->get($request->request->get('id')) :
					new User() ;
				if (!$user) {
					$json = $app->json(array(
						'status' => 'KO',
						'error' => 'Pas d’utilisateur enregistré avec l’id '.$request->request->get('id')
					), 400);
				}
				else {
					$groups = explode(',', $request->request->get('usergroup'));
					$user->setName($request->request->get('username'))
							 ->setColor($request->request->get('usercolor'))
							 ->setGroups($groups);
					$app['dao.user']->save($user);
					$groups = implode(',', $user->getGroups());
					$result = array(
						'id' => $user->getId(),
						'username' => $user->getName(),
						'usercolor' => $user->getColor(),
						'usergroup' => $groups
					);
					$json = $this->successfulOperation($result, $app);
				}
			}
			$this->logJSON($json, $app);
			return $json;
		}

		/**
		 * Parse a JSON string and create a depense entry in database from it.
		 *
		 * @param Request     $request The request sent to the application.
		 * @param Application $app     The running application.
		 *
		 * @return string A JSON string stating that the operation is a success or
		 *                giving an error message.
		 */
		public function addDepense(Request $request, Application $app) {
			$json = $this->isJSONLoggedIn($request, $app);
			if ($json === NULL ) {
				$params = [
					'montant',
					'payeur',
					'concernes',
					'usergroup',
					'description'
				];
				$json = $this->missingParameter($params, $request, $app);
			}
			if ($json === NULL ) $json = $this->wrongGroup($request, $app);
			if ($json === NULL ) {
				$depense = ($request->request->has('id')) ?
					$app['dao.depense']->get($request->request->get('id')) :
					new Depense() ;
				$date = ($request->request->has('date')) ?
					$request->request->get('date') :
					time() ;
				$users = explode(',', $request->request->get('concernes'));
				$depense->setMontant($request->request->get('montant'))
				        ->setUserId($request->request->get('payeur'))
				        ->setUsers($users)
				        ->setDate($date)
				        ->setGroupId($request->request->get('usergroup'))
				        ->setName($request->request->get('description'));
				$app['dao.depense']->save($depense);
				$users = implode(',', $depense->getUsers());
				$result = array(
					'id' => $depense->getId(),
					'montant' => $depense->getMontant(),
					'payeur' => $depense->getUserId(),
					'concernes' => $users,
					'date' => $depense->getDate(),
					'usergroup' => $depense->getGroupId(),
					'description' => $depense->getName()
				);
				$json = $this->successfulOperation($result, $app);
			}
			$this->logJSON($json, $app);
			return $json;
		}

	}

?>
