# Compta

## Demo

[Docusland.fr](http://compta.docusland.fr)

## Dépendances
PHP, Composer

Ubuntu :

	$ sudo apt install php php-fpm composer

## Installation

Clonez la branche "1.4" du dépôt

Lancez le script 'install.sh' pour installer les dépendances de l’application et régler les droits sur les fichiers :

	$ ./install.sh

Renommez le fichier app/config/dev.php.example en app/config/dev.php et renseignez-y les identifiants permettant d’accéder à votre base de données. Dans ce même fichier renseignez le login et le mot de passe du compte d’administration, ainsi que les chemins des fichiers où sont enregistrés les journaux ainsi que les clés d’authentification.

Importez le fichier sql/structure.sql dans votre base pour créer la structure attendue par l’application. Optionnellement, vous pouvez importer par la suite le fichier sql/content.sql pour peupler les tables avec des données de test.

Assignez un hôte à cette application, pointant sur web/index.php

Nginx/PHP7 :

	server {
		listen 80;
		listen [::]:80;

		root /srv/http/silex/compta-back/web;

		index index.php;

		server_name compta;

		location / {
			autoindex off;
			try_files $uri $uri/ /index.php;
		}

		location ~ /index\.php(/|$) {
			include snippets/fastcgi-php.conf;
			fastcgi_pass unix:/run/php/php7.0-fpm.sock;
			fastcgi_split_path_info ^(.+\.php)(/.*)$;
		}
	}

## Utilisation de l'API

### Route /api/groups (GET)

Renvoie la liste des groupes existants, dans un format similaire à celui-ci :

	{
		"records": [
			{
				"id": "1",
				"namegroup": "Ete 2016"
			},
			{
				"id": "9",
				"namegroup": "group_test2"
			},
			{
				"id": "13",
				"namegroup": "group_test3"
			}
		],
		"status": "OK"
	}

### Route /api/users (GET)

Renvoie la liste de tous les utilisateurs existants, dans un format similaire à celui-ci :

	{
		"records": [
			{
				"id": "66",
				"username": "Eszter",
				"usergroup": "1",
				"usercolor": "blue"
			},
			{
				"id": "67",
				"username": "Erwann",
				"usergroup": "9",
				"usercolor": "blue"
			},
			{
				"id": "82",
				"username": "Benj",
				"usergroup": "1",
				"usercolor": "red"
			}
		],
		"status": "OK"
	}

### Route /api/depenses (GET)

Renvoie la liste de toutes les dépenses existantes, dans un format similaire à celui-ci :

	{
		"records": [
			{
				"id": "268",
				"montant": "666.00",
				"payeur": "67",
				"concernes": "66,67,82,83,87,95,96,97,100,101,102,103",
				"date": "0",
				"usergroup": "1",
				"description": "Les courses du diable"
			},
			{
				"id": "269",
				"montant": "65.00",
				"payeur": "83",
				"concernes": "66",
				"date": "0",
				"usergroup": "9",
				"description": "Vaccin et croquettes"
			},
			{
				"id": "270",
				"montant": "78.00",
				"payeur": "97",
				"concernes": "66,67,82,83,87,95,96,97,100,101,102,103",
				"date": "0",
				"usergroup": "1",
				"description": "Courses du samedi soir"
			}
		],
		"status": "OK"
	}

### Route /api/group/{id}/users (GET)

Renvoie la liste des utilisateurs appartenant au groupe avec l’id {id}, dans un format similaire à celui-ci :

	{
		"records": [
			{
				"id": "66",
				"username": "Eszter",
				"usergroup": "1",
				"usercolor": "blue"
			},
			{
				"id": "82",
				"username": "Benj",
				"usergroup": "1",
				"usercolor": "red"
			},
			{
				"id": "83",
				"username": "Aurele",
				"usergroup": "1,9",
				"usercolor": "red"
			}
		],
		"status": "OK"
	}

### Route /api/group/{id}/depenses (GET)

Renvoie la liste des dépenses concernant au groupe avec l’id {id}, dans un format similaire à celui-ci :

	{
		"records": [
			{
				"id": "268",
				"montant": "666.00",
				"payeur": "67",
				"concernes": "66,67,82,83,87,95,96,97,100,101,102,103",
				"date": "0",
				"usergroup": "1",
				"description": "Les courses du diable"
			},
			{
				"id": "270",
				"montant": "78.00",
				"payeur": "97",
				"concernes": "66,67,82,83,87,95,96,97,100,101,102,103",
				"date": "0",
				"usergroup": "1",
				"description": "Courses du samedi soir"
			},
			{
				"id": "271",
				"montant": "100.00",
				"payeur": "87",
				"concernes": "67",
				"date": "0",
				"usergroup": "1",
				"description": "Dette des dernières vacances"
			}
		],
		"status": "OK"
	}

### Route /api/login (POST)

Demande une authentification en tant que session d’administration. Cette requête doit fournir un fichier json d’un format similaire à celui-ci :

	{
		"name": "admin_name",
		"password": "admin_password"
	}

Un retour contenant une clé d’API valable pour 25 minutes vous sera renvoyée, dans un format similaire à celui-ci :

	{
		"key": "c0hjmdQfUL7PvpDq7KgmDeA/QELP8kWEzZpUSVMIIWgJA+2XMjd8GfYOcEWbdZPeyxN85HoWVXvgQf2sI074yg==",
		"status": "OK"
	}

### Route /api/logout (GET)

Demande la révocation d’une clé d’API. Cette requête doit fournir la clé dans un header HTTP nommé 'apikey'.

### Route /admin/group (POST)

Cette requête doit fournir une clé d’API valide dans un header HTTP nommé 'apikey'.

Ajoute un nouveau groupe à l’application. Cette requête doit fournir un fichier json d’un format similaire à celui-ci :

	{
		"namegroup": "Printemps 2015"
	}

Modifie un groupe existant. Cette requête doit fournir un fichier json d’un format similaire à celui-ci :

	{
		"id": "2"
		"namegroup": "Automne 2014"
	}

### Route /admin/user (POST)

Cette requête doit fournir une clé d’API valide dans un header HTTP nommé 'apikey'.

Ajoute un nouvel utilisateur à l’application. Cette requête doit fournir un fichier json d’un format similaire à celui-ci :

	{
		"username": "Aurele",
		"usergroup": "1",
		"usercolor": "red"
	}

Modifie un utilisateur existant. Cette requête doit fournir un fichier json d’un format similaire à celui-ci :

	{
		"id": "87",
		"username": "Momo",
		"usergroup": "1",
		"usercolor": "black"
	}

### Route /admin/depense (POST)

Cette requête doit fournir une clé d’API valide dans un header HTTP nommé 'apikey'.

Ajoute une nouvelle dépense à l’application. Cette requête doit fournir un fichier json d’un format similaire à celui-ci :

	{
		"montant": "78.00",
		"payeur": "97",
		"concernes": "66,67,82,83,87,95,96,97,100,101,102,103",
		"date": "0",
		"usergroup": "1",
		"description": "Courses du samedi soir"
	}

Modifie une dépense existante. Cette requête doit fournir un fichier json d’un format similaire à celui-ci :

	{
		"id": "271",
		"montant": "100.00",
		"payeur": "87",
		"concernes": "67",
		"date": "0",
		"usergroup": "1",
		"description": "Dette des dernières vacances"
	}

### Route /admin/group/{id} (DELETE)

Cette requête doit fournir une clé d’API valide dans un header HTTP nommé 'apikey'.

Supprime le groupe avec l’id {id}. Les dépenses associées à ce groupe ainsi que les utilisateurs n’appartenant qu’à ce groupe seront automatiquement supprimés.

### Route /admin/user/{id} (DELETE)

Cette requête doit fournir une clé d’API valide dans un header HTTP nommé 'apikey'.

Supprime l’utilisateur avec l’id {id}. Les dépenses dont cet utilisateur est le payeur ou le seul concerné seront automatiquement supprimées.

### Route /admin/depense/{id} (DELETE)

Cette requête doit fournir une clé d’API valide dans un header HTTP nommé 'apikey'.

Supprime la dépense avec l’id {id}.

## Bugs
Cette application n'est pas maintenue par une communauté. 
Si vous le souhaitez vous pouvez tout de même envoyer vos rapports de bugs ou demandes d’information sur le bug tracker du projet.
Dans votre rapport précisez la version de l’application utilisée, et joignez les 10 dernières lignes du fichier logs/dev.log
