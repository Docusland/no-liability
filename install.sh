#!/bin/sh
set -e

# check required dependencies
check_deps() {
	for dep in $@; do
		if [ -z $(which "${dep}") ]; then
			printf '\n\033[1;31mErreur :\033[0m\n'
			printf '%s est introuvable.\n' "${dep}"
			printf 'Installez-le avant de lancer ce script.\n\n'
			exit 1
		fi
	done
}

# fix rights on cache and logs dirs
check_deps 'groups' 'chgrp' 'chmod'
if [ -n "$(groups|grep 'www-data')" ]; then
	chgrp -c www-data ./cache ./logs
	chmod -c g+w ./cache ./logs
elif [ -n "$(which sudo)" ]; then
	sudo chgrp -c www-data ./cache ./logs
	sudo chmod -c g+w ./cache ./logs
else
	printf '\n\033[1;33mAvertissement :\033[0m\n'
	printf 'Vous devez donner les droits d’écriture au groupe www-data sur les'
	printf 'répertoires ./cache et ./logs avant de lancer cette application.\n'
fi

# install app dependencies
check_deps 'composer'
composer update

# generate documentation
#check_deps 'apigen'
#apigen generate -s ./src/ -d ./web/docs/

exit 0
